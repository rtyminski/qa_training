import requests
import config

class TestIndexPage:
    def test_index_page_is_loaded(self):
        #When
        response = requests.get(config.base_url)
        # Then
        assert response.status_code == 200
        assert 'Hello, World' in response.text


