import pytest
import requests
from faker import Faker

import endpoints

fake = Faker()

@pytest.fixture
def new_user_data() -> dict:
        expected_email = fake.safe_email()
        payload = {'email': expected_email}

        response = requests.post(endpoints.users, json=payload)

        assert response.status_code == 201
        json = response.json()
        assert json['email'] == expected_email
        assert 'id' in json

        return {'email': expected_email, 'id': json['id']}

