import requests

import endpoints

class TestUsers:
    def test_get_users_list(self, new_user_data):
        #When
        response = requests.get(endpoints.users)
        # Then
        assert response.status_code == 200
        assert new_user_data in response.json()

    def test_create_user_and_get_their_details(self, new_user_data):
        id = new_user_data['id']
        response = requests.get(endpoints.user_with_id(id))

        assert response.status_code == 200
        json = response.json()
        assert json['email'] == new_user_data['email']
        assert json['id'] == id

