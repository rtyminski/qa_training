Faker==18.9.0
requests==2.31.0
pytest==7.3.1
allure-pytest==2.13.2
pytest-html==3.2.0
